import { ZCore } from '@zoib/core';
import { ZLogger } from '@zoib-utils/logger';

function wrapWithErrorLogger(fn) {
    return _ => {
        return fn().catch((err) => {
            logger.error(err);
        });
    };
}

const app = express();
const zCore = new ZCore();

zCore.logger = new ZLogger();
zCore.httpFramework = app;

const option = {
    serverHttp: {
        enabled: true,
        framework: ZCore.HTTP_FRAMEWORK.EXPRESS,
    },
    modules: [
        
    ],
};

zCore.load(option).then(_ => {
    process.on('uncaughtException', (err) => {
        zCore.logger.error('Process receive Uncaught Exception');
        if (err.stack) {
            zCore.logger.error(err.stack);
        }
        else {
            zCore.logger.error(err);
        }
    });
    
    process.on('SIGTERM', _ => {
        zCore.logger.info('Process receive SIGTERM');
        wrapWithErrorLogger(_ => {
            return stopServer(true);
        })();
    });
    
    process.on('SIGINT', _ => {
        zCore.logger.info('Process receive SIGINT');
        wrapWithErrorLogger(_ => {
            return zCore.stopServer(true);
        })();
    });
    
    wrapWithErrorLogger(() => {
        return zCore.startServer();
    })();
});
